import React from 'react';

class Titles extends React.Component {

    render() {
        return (
            <div>
                <h1 className="title-container__title">Weather Finder</h1>
                <h3 className="title-container__subtitle">Find out temperature, conditions and more...</h3>
            </div>
        );
    }

}

const styles = {
    header: {
        paddingTop: 30,
        paddingBottom: 30,
        backgroundColor: '#595478',
        color: 'white',
        textAlign: 'center',
    }
}
export default Titles;